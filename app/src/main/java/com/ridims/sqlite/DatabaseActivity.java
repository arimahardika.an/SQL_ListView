package com.ridims.sqlite;

import android.app.Activity;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.Handler;
import android.os.PersistableBundle;
import android.preference.PreferenceManager;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.widget.ListView;
import android.widget.TextView;

import java.util.ArrayList;


/**
 * Created on 01/06/2016 by
 * Name     : Ari Mahardika Ahmad Nafis
 * Email    : arimahardika.an@gmail.com
 */
public class DatabaseActivity extends AppCompatActivity {

    DBHelper dbHelper;

    ListView listView;

    NameAdapter nameAdapter;

    ArrayList<String> personListString; //
    ArrayList<Person> personList = new ArrayList<>(); //
    String[] nama = new String[personList.size()]; //

    @Override
    protected void onCreate( Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.show);
        listView = (ListView) findViewById(R.id.listView);

        dbHelper = new DBHelper(this);
        /*Person person = new Person("Ari", "Batu");
        dbHelper.addPerson(person);*/

        personListString = dbHelper.getAllN(); //
        personList = dbHelper.getAll(); //

        nameAdapter = new NameAdapter(this, android.R.layout.simple_list_item_1, personListString); //

        if(personListString.size() > 0){ //
            listView.setAdapter(nameAdapter); //
        } //



        //listView = (ListView) findViewById(R.id.listView);

        //ArrayList<Person> arrayList = dbHelper.getAll();
        //if(arrayList.size()>0){
        //    Person per = arrayList.get(0);
        //    textView.setText(per.name + " "+per.address);
        //}
    }
}
