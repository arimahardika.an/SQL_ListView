package com.ridims.sqlite;

/**
 * Created on 01/06/2016 by
 * Name     : Ari Mahardika Ahmad Nafis
 * Email    : arimahardika.an@gmail.com
 */
public class Person {
    Integer id;
    String name;
    String address;

    public Person() {

    }

    public Person(String name, String address) {
        this.name = name;
        this.address = address;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }
}
