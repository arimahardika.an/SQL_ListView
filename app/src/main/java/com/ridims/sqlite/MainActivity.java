package com.ridims.sqlite;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

public class MainActivity extends AppCompatActivity {

    Button button;
    EditText et1, et2;
    DBHelper dbHelper;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        et1 = (EditText) findViewById(R.id.text_1);
        et2 = (EditText) findViewById(R.id.text_2);
        dbHelper = new DBHelper(this);

        button = (Button) findViewById(R.id.btn_submit);

        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(MainActivity.this, DatabaseActivity.class);
                Person person = new Person(et1.getText().toString(), et2.getText().toString());
                dbHelper.addPerson(person);
                startActivity(intent);
            }
        });
    }
}
