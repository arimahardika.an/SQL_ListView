package com.ridims.sqlite;

import android.content.ContentValues;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import java.util.ArrayList;

/**
 * Created on 01/06/2016 by
 * Name     : Ari Mahardika Ahmad Nafis
 * Email    : arimahardika.an@gmail.com
 */
public class DBHelper extends SQLiteOpenHelper{

    public static String DATABASE_NAME = "MyDb.db";
    public static String PERSON_TABLE_NAME = "person";
    public static String PERSON_COLUMN_ID = "id";
    public static String PERSON_COLUMN_NAME = "nama";
    public static String PERSON_COLUMN_ADDRESS = "address";

    public DBHelper(Context context){
        super(context, DATABASE_NAME, null, 1);
    }

    @Override
    public void onCreate(SQLiteDatabase db){
        db.execSQL("Create table " +PERSON_TABLE_NAME + "(id integer primary key, nama text, address text)");
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL("drop table if exist" + PERSON_TABLE_NAME);
    }

    public void addPerson(Person person){
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues contentValues = new ContentValues();
        contentValues.put("nama", person.getName());
        contentValues.put("address", person.getAddress());
        db.insert(PERSON_TABLE_NAME, null, contentValues);
    }

    public void updatePerson(Integer id, Person person){
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues contentValues = new ContentValues();
        contentValues.put("nama", person.getName());
        contentValues.put("address", person.getAddress());
        db.update(PERSON_TABLE_NAME, contentValues, "id = ?",
                new String[]{Integer.toString(id)});
    }

    public void deletePerson(Integer id){
        SQLiteDatabase db = this.getWritableDatabase();
        db.delete(PERSON_TABLE_NAME, "id = ?", new String[]{Integer.toString(id)});
    }

    public ArrayList<Person> getAll(){

        ArrayList<Person> arrayList = new ArrayList<>();
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.rawQuery("select * from " +PERSON_TABLE_NAME, null);
        cursor.moveToFirst();

        while(cursor.isAfterLast() == false){
            Person person = new Person();
            person.setId(cursor.getInt(0));
            person.setName(cursor.getString(1));
            person.setAddress(cursor.getString(2));

            arrayList.add(person);
            cursor.moveToNext();
        }
        return arrayList;
    }

    public ArrayList<String> getAllN(){
        ArrayList<String> arrayList = new ArrayList<>();
        SQLiteDatabase sqLiteDatabase = this.getReadableDatabase();
        Cursor cursor = sqLiteDatabase.rawQuery("select * from " + PERSON_TABLE_NAME, null);
        cursor.moveToFirst();

        while (cursor.isAfterLast() == false) {
            Person person = new Person();
            person.setId(cursor.getInt(0));
            person.setName(cursor.getString(1));
            person.setAddress(cursor.getString(2));

            //add into arrayList
            arrayList.add(cursor.getString(1));
            cursor.moveToNext();
        }
        return arrayList;
    }

    public Person getPerson(int id){
        Person person = new Person();
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.rawQuery("select * from " + PERSON_TABLE_NAME +" "+"where id = "+id, null);
        cursor.moveToFirst();

        if(cursor.getCount() <= 0){
            return null;
        }
        person.setId(cursor.getInt(0));
        person.setName(cursor.getString(1));
        person.setAddress(cursor.getString(2));

        return person;
    }

    public Person getName(String name){
        Person person = new Person();
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.rawQuery("select name from " + PERSON_TABLE_NAME +" "+"where name = "+name, null);
        cursor.moveToFirst();

        if(cursor.getCount() <= 0){
            return null;
        }
        person.setId(cursor.getInt(0));
        person.setName(cursor.getString(1));

        return person;

    }


}
